When updating the exodus library source, the exodus_c_binding.F90 file in
the pececillo source tree must be updated to match the definitions from
the new library's exodusII.h header file.
