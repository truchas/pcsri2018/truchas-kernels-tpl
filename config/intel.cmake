# Generic Linux with the Intel Compilers
# The compiler executables icc and ifort must be in your path.

set(CMAKE_BUILD_TYPE Release CACHE STRING "Build type")
set(CMAKE_C_COMPILER icc CACHE STRING "C Compiler")
set(CMAKE_Fortran_COMPILER ifort CACHE STRING "Fortran Compiler")
set(ENABLE_MPI NO CACHE BOOL "Parallel libraries")
set(ENABLE_OPENMP NO CACHE BOOL "Use OpenMP")
