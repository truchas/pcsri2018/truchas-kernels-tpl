Third Party Software
--------------------
The tarfiles subdirectory contains following third-party software:

Software      | License      | Author/Source
--------------|--------------|--------------
ExodusII 5.14 | BSD          | https://sourceforge.net/projects/exodusii
HDF5 1.8.11   | BSD-style    | http://www.hdfgroup.org/HDF5
NetCDF 4.1.3  | BSD-style    | http://www.unidata.ucar.edu/software/netcdf
Petaca        | MIT          | https://github.com/nncarlson/petaca
YAJL 2.0.4    | MIT-style    | https://github.com/lloyd/yajl
